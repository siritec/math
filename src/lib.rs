extern crate statrs;

use statrs::statistics::Statistics;
use statrs::function::erf::*;
use std::f64::NAN;

pub fn stdev(v: &[f64]) -> f64 {
    Statistics::variance(v).sqrt()
}

fn ppoints(v: &[f64]) -> Vec<f64> {
    let l = v.len();

    // The def of a is by definition
    let a = if l <= 10 { 0.375 } else { 0.5 };

    (0..l).map(|x| { (x as f64 + 1. - a) / (l as f64 + 1. - a * 2.) }).collect()
}

pub fn quantile(p: f64) -> f64 {
    - erfc_inv(2f64 * p) * 1.414213562373095
}

pub fn normalize(v: &[f64]) -> Vec<f64> {
    let v_valid = v.iter().filter(|x| x.is_finite()).cloned().collect::<Vec<_>>();

    let pp = ppoints(&v_valid);

    let mut position: Vec<usize> = (0..v_valid.len()).collect();
    let mut q = vec![0.0; v_valid.len()];

    unsafe {
        position.sort_unstable_by(|&k1, &k2| {
            v_valid.get_unchecked(k1)
                .partial_cmp(v.get_unchecked(k2))
                .unwrap()
        });

        position.iter().enumerate().for_each(|(k, &v_valid)|{
            *q.get_unchecked_mut(v_valid) = quantile(pp[k])
        })
    }

    let mut res = Vec::with_capacity(v.len());
    let mut q_iter = q.into_iter();

    for i in 0..v.len() {
        if v[i].is_finite() {
            res.push(q_iter.next().unwrap())
        } else {
            res.push(NAN)
        }
    }
    
    // one liner: to check performance
    // let res = v.iter().cloned().collect::<Vec<_>>();
    // res.iter_mut().filter(|x| x.is_finite()).zip(qpp.iter()).for_each(|(x1, x2)| *x1 = *2)

    res
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        use super::normalize;
        use std::f64::NAN;

        let v = vec![0.2, -0.1, NAN, 0.3];
        let l = normalize(&v);
        let r = vec![-0.0, -0.8694237732888856, NAN, 0.869423773288886];

        for k in l.iter().zip(&r) {
            if k.0.is_nan() && k.1.is_nan() {
                continue
            } else {
                assert_eq!(k.0, k.1)
            }
        }
    }
}
